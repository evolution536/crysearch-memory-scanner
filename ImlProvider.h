#ifndef _CrySearch_ImlProvider_h_
#define _CrySearch_ImlProvider_h_

// IML file loader for pictures inside the program.
// Include this file in every CPP file that requires imaging.
#define IMAGECLASS CrySearchIml
#define IMAGEFILE "CrySearch.iml"
#include <Draw/iml_header.h>

#endif