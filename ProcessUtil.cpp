#include "ProcessUtil.h"
#include "NativeAPI.h"

#include <Psapi.h>
#include <VerRsrc.h>
#include <DbgHelp.h>

#include "BackendGlobalDef.h"

// Enumerates every process currently running.
void EnumerateProcesses(Vector<Win32ProcessInformation>& outList)
{
	outList.Clear();
	
	NTSTATUS returnVal;
	ULONG dataLength = 0x10000;
	PSYSTEM_PROCESS_INFORMATION procInfo = NULL;
	
	// Query the system processes. If the call fails because of a length mismatch, recreate a bigger buffer and try again.
	do
	{
		procInfo = (PSYSTEM_PROCESS_INFORMATION)VirtualAlloc(NULL, dataLength, MEM_COMMIT, PAGE_READWRITE);
		returnVal = CrySearchRoutines.NtQuerySystemInformation(SystemExtendedProcessInformation, procInfo, dataLength, &dataLength);
		if (returnVal == STATUS_INFO_LENGTH_MISMATCH)
		{
			// The length of the buffer was not sufficient. Expand the buffer before retrying.
			VirtualFree(procInfo, 0, MEM_RELEASE);
			dataLength *= 2;
		}
	}
	while (returnVal == STATUS_INFO_LENGTH_MISMATCH);
	
	// Did the query succeed?
	if (returnVal == STATUS_SUCCESS)
	{
		PSYSTEM_PROCESS_INFORMATION curProc = procInfo;
		do
		{
			curProc = (PSYSTEM_PROCESS_INFORMATION)((Byte*)curProc + curProc->NextEntryOffset);
			
			// Skip the first two system processes.
			if ((int)curProc->UniqueProcessId > 4)
			{
				Win32ProcessInformation wpi;
				wpi.ProcessId = (int)curProc->UniqueProcessId;
				wpi.ExeTitle = WString(curProc->ImageName.Buffer, curProc->ImageName.Length / 2).ToString();

				outList.Add(wpi);
			}
		}
		while (curProc->NextEntryOffset);
	}
	
	// Free heap allocated process information.
	VirtualFree(procInfo, 0, MEM_RELEASE);
}

// Retrieves all threads loaded from a process.
void EnumerateThreads(const int processId, Vector<Win32ThreadInformation>& threads)
{
	threads.Clear();
	
	NTSTATUS returnVal;
	ULONG dataLength = 0x10000;
	PSYSTEM_PROCESS_INFORMATION procInfo = NULL;
	
	// Query the system processes. If the call fails because of a length mismatch, recreate a bigger buffer and try again.
	do
	{
		procInfo = (PSYSTEM_PROCESS_INFORMATION)VirtualAlloc(NULL, dataLength, MEM_COMMIT, PAGE_READWRITE);
		returnVal = CrySearchRoutines.NtQuerySystemInformation(SystemExtendedProcessInformation, procInfo, dataLength, &dataLength);
		if (returnVal == STATUS_INFO_LENGTH_MISMATCH)
		{
			// The length of the buffer was not sufficient. Expand the buffer before retrying.
			VirtualFree(procInfo, 0, MEM_RELEASE);
			dataLength *= 2;
		}
	}
	while (returnVal == STATUS_INFO_LENGTH_MISMATCH);
	
	// Did the query succeed?
	if (returnVal == STATUS_SUCCESS)
	{
		PSYSTEM_PROCESS_INFORMATION curProc = procInfo;
		do
		{
			curProc = (PSYSTEM_PROCESS_INFORMATION)((Byte*)curProc + curProc->NextEntryOffset);
			
			// Iterate processes until the correct one is found.
			if ((int)curProc->UniqueProcessId == processId)
			{
				// Iterate threads of process.
				for (DWORD t = 0; t < curProc->NumberOfThreads; ++t)
				{
					PSYSTEM_EXTENDED_THREAD_INFORMATION curThread = &curProc->Threads[t];
					Win32ThreadInformation& newEntry = threads.Add();
					newEntry.ThreadIdentifier = (int)curThread->ThreadInfo.ClientId.UniqueThread;
					newEntry.StartAddress = curThread->Win32StartAddress ? (SIZE_T)curThread->Win32StartAddress : (SIZE_T)curThread->ThreadInfo.StartAddress;
					newEntry.IsSuspended = curThread->ThreadInfo.WaitReason == Suspended;
				}
			}
		}
		while (curProc->NextEntryOffset);
	}
	
	// Free heap allocated process information.
	VirtualFree(procInfo, 0, MEM_RELEASE);
}

// Enumerates heaps inside the target process.
// Returns true if the function succeeded or false if it did not.
bool EnumerateHeaps(Vector<Win32HeapInformation>& heapInfoList)
{
	// Create debug buffer to hold heap information.
	PRTL_DEBUG_INFORMATION db = CrySearchRoutines.RtlCreateQueryDebugBuffer(0, FALSE);
	if (!db)
	{
		return false;
	}
	
	// Get heap information and put it inside the debug buffer.
	NTSTATUS result = CrySearchRoutines.RtlQueryProcessDebugInformation(mMemoryScanner->GetProcessId(), PDI_HEAPS | PDI_HEAP_BLOCKS, db);
	if (result != STATUS_SUCCESS)
	{
		CrySearchRoutines.RtlDestroyQueryDebugBuffer(db);
		return false;
	}
	
	// Walk the heaps and add them to the input vector.
	for (unsigned int i = 0; i < db->Heaps->NumberOfHeaps; ++i)
	{
		PRTL_HEAP_INFORMATION curHeap = &db->Heaps->Heaps[i];
		Win32HeapInformation& heap = heapInfoList.Add();
		heap.VirtualAddress = (LONG_PTR)curHeap->BaseAddress;
		heap.BlockCount = curHeap->NumberOfEntries;
		heap.CommittedSize = (LONG)curHeap->BytesCommitted;
		heap.AllocatedSize = (LONG)curHeap->BytesAllocated;
		heap.Flags = curHeap->Flags;
	}

	// Clean up buffer and return.
	CrySearchRoutines.RtlDestroyQueryDebugBuffer(db);
	return true;
}

// Retrieves the foreign name of an object based on the type.
// Returns its name if the function succeeded. Otherwise the return value is an empty string.
String GetObjectNameByType(HANDLE hObject, const wchar* pType, const DWORD length)
{
	String retVal;
	
	if (wcsncmp(pType, L"Process", length) == 0)
	{
		// The object is a process, get its filename.
		char path[MAX_PATH];
		if (GetProcessImageFileName(hObject, path, MAX_PATH))
		{
			retVal = Format("(%i) - %s", (int)GetProcessId(hObject), GetFileName(path));
		}
	}
	else if (wcsncmp(pType, L"Thread", length) == 0)
	{
		// The object is a thread, get its thread ID.
		THREAD_BASIC_INFORMATION ti;
		if (CrySearchRoutines.NtQueryInformationThread(hObject, ThreadBasicInformation, &ti, sizeof(THREAD_BASIC_INFORMATION), NULL) == STATUS_SUCCESS)
		{
			retVal = Format("Thread ID: %i", (int)ti.ClientId.UniqueThread);
		}
	}
	else
	{
		// The object was not a user-friendly object that can be queried with logical information. Call NtQueryObject.
		// Allocate a buffer to hold the object name and query it into the buffer.
		POBJECT_NAME_INFORMATION objNameInfo = (POBJECT_NAME_INFORMATION)VirtualAlloc(NULL, 0x1000, MEM_COMMIT, PAGE_READWRITE);
		ULONG outLength;
		NTSTATUS statusCode = CrySearchRoutines.NtQueryObject(hObject, ObjectNameInformation, objNameInfo, 0x1000, &outLength);
		if (statusCode != STATUS_SUCCESS)
		{
			// The query failed, the buffer may have been too small.
			VirtualFree(objNameInfo, 0, MEM_RELEASE);
			objNameInfo = (POBJECT_NAME_INFORMATION)VirtualAlloc(NULL, outLength, MEM_COMMIT, PAGE_READWRITE);
			
			// Retry the query with the adjusted buffer size.
			statusCode = CrySearchRoutines.NtQueryObject(hObject, ObjectNameInformation, objNameInfo, outLength, &outLength);
		}
		
		// Query succeeded?
		if (statusCode == STATUS_SUCCESS)
		{
			// Not all handles have a name. Sanity check for the name.
			if (objNameInfo->ObjectName.Buffer)
			{
				retVal = WString(objNameInfo->ObjectName.Buffer, objNameInfo->ObjectName.Length + 1).ToString();
			}
		}
		
		// Free the buffer allocated to hold the name.
		VirtualFree(objNameInfo, 0, MEM_RELEASE);
	}
	
	return retVal;
}

// Enumerates the heaps inside the target process. This function does not return a value.
// When it succeeds, the out Vector parameter contains the handles. If failed, the Vector is empty.
void EnumerateHandles(const int processId, Vector<Win32HandleInformation>& handles)
{
	// Clear the output Vector.
	handles.Clear();
	
	NTSTATUS returnVal;
	ULONG dataLength = 0x10000;
	PSYSTEM_HANDLE_INFORMATION handleInfo = NULL;
	
	// Query the system handles. If the call fails because of a length mismatch, recreate a bigger buffer and try again.
	do
	{
		handleInfo = (PSYSTEM_HANDLE_INFORMATION)VirtualAlloc(NULL, dataLength, MEM_COMMIT, PAGE_READWRITE);
		returnVal = CrySearchRoutines.NtQuerySystemInformation(SystemHandleInformation, handleInfo, dataLength, &dataLength);
		if (returnVal == STATUS_INFO_LENGTH_MISMATCH)
		{
			// The length of the buffer was not sufficient. Expand the buffer before retrying.
			VirtualFree(handleInfo, 0, MEM_RELEASE);
			dataLength *= 2;
		}
	}
	while (returnVal == STATUS_INFO_LENGTH_MISMATCH);
	
	// Did the query succeed?
	if (returnVal == STATUS_SUCCESS)
	{
		// The system query succeeded, let's allocate buffers to hold the necessary information.
		POBJECT_TYPE_INFORMATION objInfo = (POBJECT_TYPE_INFORMATION)VirtualAlloc(NULL, 0x1000, MEM_COMMIT, PAGE_READWRITE);
		POBJECT_BASIC_INFORMATION objBasicInfo = (POBJECT_BASIC_INFORMATION)VirtualAlloc(NULL, sizeof(OBJECT_BASIC_INFORMATION), MEM_COMMIT, PAGE_READWRITE);
		
		// The count is available, let's resize the Vector to save us the additional allocations.
		handles.Reserve(handleInfo->NumberOfHandles);
		
		// Walk the retrieved handle collection.
		for (DWORD i = 0; i < handleInfo->NumberOfHandles; ++i)
		{
			const PSYSTEM_HANDLE_TABLE_ENTRY_INFO curHandle = &handleInfo->Handles[i];
			
			// Does this handle belong to our process?
			if (curHandle->UniqueProcessId == processId)
			{
				// Duplicate the handle in order to find out what object it is associated with.
				HANDLE hDup;
				DuplicateHandle(mMemoryScanner->GetHandle(), (HANDLE)curHandle->HandleValue, GetCurrentProcess(), &hDup, 0, FALSE, DUPLICATE_SAME_ACCESS);
				
				// Check if the handle was succesfully duplicated. StdHandle's cannot be duplicated for instance.
				if (GetLastError() != ERROR_NOT_SUPPORTED)
				{
					// Query the object to find out what kind of object it is.
					if (CrySearchRoutines.NtQueryObject(hDup, ObjectTypeInformation, objInfo, 0x1000, NULL) == STATUS_SUCCESS)
					{
						// Add new handle to the list.
						Win32HandleInformation& newHandle = handles.Add();
						newHandle.Handle = curHandle->HandleValue;
						newHandle.Access = curHandle->GrantedAccess;
						newHandle.ObjectType = WString(objInfo->TypeName.Buffer, objInfo->TypeName.Length).ToString();

						// When calling NtQueryObject with ObjectNameInformation, the function may never return if the access mask is 0x0012019F.
						// We cannot query the name of these handles. In my case, 0x00120089 and 0x0012008D are also culprits.
						newHandle.ObjectName = (curHandle->GrantedAccess == 0x00120089 || curHandle->GrantedAccess == 0x0012019F || curHandle->GrantedAccess == 0x0012008D)
							? "" : GetObjectNameByType(hDup, objInfo->TypeName.Buffer, objInfo->TypeName.Length + 1);
						
						// If the handle refers to a file, we can query the exact path on the filesystem with another query.
						if (wcsncmp(objInfo->TypeName.Buffer, L"File", objInfo->TypeName.Length + 1) == 0)
						{
							// In case of a file, we can try to retrieve the mapped drive letter.
							const char driveLetter = GetMappedDriveLetter(newHandle.ObjectName, newHandle.ObjectName.GetLength());
							
							// Did we get a sensible drive letter?
							if (driveLetter)
							{
								// Strip the device parts from the path. We assume that a native device path always has two parts up front.
								String procPath = newHandle.ObjectName;
								
								// Remove trailing backslash.
								procPath.Remove(0);
								
								// Remove the first part, including the next backslash.
								int bkslashIndex = procPath.FindFirstOf("\\") + 1;
								if (bkslashIndex >= 0 && bkslashIndex < procPath.GetLength())
								{
									procPath.Remove(0, bkslashIndex);
								}
								
								// Remove the second part.
								bkslashIndex = procPath.FindFirstOf("\\");
								if (bkslashIndex >= 0 && bkslashIndex < procPath.GetLength())
								{
									procPath.Remove(0, bkslashIndex);
								}
								
								// We now have the real path, we can prepend the drive letter.
								String finalPath;
								finalPath += driveLetter;
								finalPath += ":" + procPath;
								
								// We should check whether the path we have just created exists, to be a 100 percent sure that what we
								// did is actually correct, since we made an assumption on the structure of the native path.
								if (DirectoryExists(finalPath) || FileExists(finalPath))
								{
									// The path exists and is correct.
									newHandle.ObjectName = finalPath;
								}
							}
						}

						// Query the object again for the other information block.
						if (CrySearchRoutines.NtQueryObject(hDup, ObjectBasicInformation, objBasicInfo, sizeof(OBJECT_BASIC_INFORMATION), &dataLength) == STATUS_SUCCESS)
						{
							// As documented in ProcessHacker, we should decrement the handle count because NtQueryObject opened a handle
							// to this object too. This handle is not applicable for counting with the references.
							newHandle.ReferenceCount = objBasicInfo->HandleCount - 1;
						}
					}
				}

				// Close the duplicate handle ofcourse.
				CloseHandle(hDup);
			}
		}
		
		// Free allocated query objects.
		VirtualFree(objBasicInfo, 0, MEM_RELEASE);
		VirtualFree(objInfo, 0, MEM_RELEASE);
	}
	
	// Free heap allocated handle information.
	VirtualFree(handleInfo, 0, MEM_RELEASE);
}

// Attempts to retrieve the name of a symbol. This function requires the SymInitialize function
// to be previously executed. Returns true if the symbol lookup succeeded and false otherwise.
const bool GetSingleSymbolName(HANDLE hProcess, const SIZE_T addrOffset, char* const outSymbolName, const DWORD bufferSize)
{
	const DWORD arrSize = sizeof(IMAGEHLP_SYMBOL64) + MAX_SYM_NAME;
	char buffer[arrSize];
	memset(buffer, 0, arrSize);

	IMAGEHLP_SYMBOL64* const symbol = (IMAGEHLP_SYMBOL64*)buffer;
	symbol->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL64);
    symbol->MaxNameLength = MAX_SYM_NAME;
    
    // Attempt to retrieve symbol name from PDB file.
    if (SymGetSymFromAddr64(hProcess, addrOffset, NULL, symbol))
    {
        UnDecorateSymbolName(symbol->Name, outSymbolName, bufferSize, UNDNAME_COMPLETE);
        return true;
    }
    else
    {
        return false;
    }
}

// Obtains the stack trace for a hit breakpoint and puts it into the last parameter.
void ConstructStackTrace(HANDLE hProcess, const DWORD machineType, const void* const contextPtr, Vector<DWORD64>& outStackTrace)
{
	outStackTrace.Clear();

	STACKFRAME64 StackFrame;
	memset(&StackFrame, 0, sizeof(STACKFRAME64));
	PVOID localCtx = NULL;
	
	if (machineType == IMAGE_FILE_MACHINE_I386)
	{
#ifdef _WIN64
		WOW64_CONTEXT ctx;
		memcpy(&ctx, contextPtr, sizeof(WOW64_CONTEXT));
#else
		CONTEXT ctx;
		memcpy(&ctx, contextPtr, sizeof(CONTEXT));
#endif
		StackFrame.AddrPC.Offset = ctx.Eip;
		StackFrame.AddrFrame.Offset = ctx.Ebp;
		StackFrame.AddrStack.Offset = ctx.Esp;
		localCtx = &ctx;
	}
#ifdef _WIN64
	else
	{
		CONTEXT ctx;
		memcpy(&ctx, contextPtr, sizeof(CONTEXT));
		StackFrame.AddrPC.Offset = ctx.Rip;
		StackFrame.AddrFrame.Offset = ctx.Rbp;
		StackFrame.AddrStack.Offset = ctx.Rsp;
		localCtx = &ctx;
	}
#endif

	StackFrame.AddrPC.Mode = AddrModeFlat;
	StackFrame.AddrFrame.Mode = AddrModeFlat;
	StackFrame.AddrStack.Mode = AddrModeFlat;
	
	BOOL result;
	
	do
	{
		// Don't forget to make a copy of the context structure. It may get modified on the way.
		result = StackWalk64(machineType, hProcess, NULL, &StackFrame, localCtx, NULL, SymFunctionTableAccess64, SymGetModuleBase64, NULL);
	
		// When we reach a return address, stop the stack walking.
		if (StackFrame.AddrPC.Offset == StackFrame.AddrReturn.Offset)
		{
			break;
		}
		
		// Add the stack trace entry to the output vector.
        outStackTrace.Add(StackFrame.AddrPC.Offset);
	}
	while (result);
}

// Customized OS version check. The default check does not work well on Windows 10.
const bool GetInlineWindowsVersion(Tuple2<int, int>* outVersion)
{
	// Retrieve the function address for the RtlGetVersion function.
	RtlGetVersionPrototype RtlGetVersion = (RtlGetVersionPrototype)GetProcAddress(GetModuleHandle("ntdll.dll"), "RtlGetVersion");
	if (!RtlGetVersion)
	{
		return false;
	}
	
	// Attempt to retrieve the OS version information.
	OSVERSIONINFOEX osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	if (RtlGetVersion(&osv) != STATUS_SUCCESS)
	{
		return false;
	}
	
	// Export the version numbers for further use.
	outVersion->a = osv.dwMajorVersion;
	outVersion->b = osv.dwMinorVersion;
	
	return true;
}